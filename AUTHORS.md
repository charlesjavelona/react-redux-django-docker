CTRL-Innovation (Sheridan College capstone group 2017)
- Ryan Falcon (Galimova) <ryan7falcon@gmail.com>
- Thomas Irwin <thomas.P.Irwin@gmail.com>
- Charles Javelona <charlesjavelona@gmail.com>
- Lizzie Urrutia <lizzie.luz@gmail.com>

The base for react and redux part of the project was created by Ryan Falcon (Galimova) <ryan7falcon@gmail.com>

The base for django and docker part of the project was created at Seedstars Labs (www.seedstarslabs.com) for internal use. 

- Daniel Silva <daniel.silva@seedstarslabs.com>
- Fernando Ferreira <fernando.ferreira@seedstarslabs.com>
- Filipe Garcia <filipe.garcia@seedstarslabs.com>
- Luis Rodrigues <luis.rodrigues@seedstarslabs.com>
- Pedro Gomes <pedro.gomes@seedstarslabs.com>
- Ruben Almeida <ruben.almeida@seedstarslabs.com>
