import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import PropTypes from 'prop-types';

import getRoutes from '../../routes';
import { AppContainer } from 'containers';

export default class RootContainer extends React.Component {
    static propTypes = {
        store: PropTypes.shape().isRequired,
        history: PropTypes.shape().isRequired
    };

    render() {
        return (
            <div>
                <Provider store={this.props.store}>
                    <AppContainer>
                        <ConnectedRouter history={this.props.history}>
                            {getRoutes(this.props.store, this.props.history)}             
                        </ConnectedRouter>
                    </AppContainer>
                </Provider>
            </div>
        );
    }
}
