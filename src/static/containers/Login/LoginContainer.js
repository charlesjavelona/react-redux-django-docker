import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import PropTypes from 'prop-types';
import * as userActionCreators from 'reduxModules/modules/users'
import { checkToken } from 'helpers/utils'
import { Login } from 'components'

class LoginContainer extends React.Component {
    static propTypes = {
        push: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool.isRequired,
        isAuthenticating: PropTypes.bool.isRequired,
        statusText: PropTypes.string,
        loginWithCredentials: PropTypes.func.isRequired,
        register: PropTypes.func.isRequired,
        location: PropTypes.shape({
            search: PropTypes.string.isRequired,
        }),
    };

    static defaultProps = {
        statusText: '',
        location: null
    };

    constructor(props) {
        super(props);

        const redirectRoute = this.props.location ? this.extractRedirect(this.props.location.search) || '/' : '/';
        this.state = {
            formValues: {
                email: '',
                password: '',
                iWantToRegister: false,
                repeatPassword: '',
                firstName: '',
                lastName: '',
            },
            error: '',
            redirectTo: redirectRoute
        };
    }

    componentWillMount() {
       this.checkAuth(this.props)   
    }

    componentWillReceiveProps(newProps) {
        this.checkAuth(newProps)   
    }

    checkAuth = (props) => {
        
        if (props.isAuthenticated && this.state.redirectTo!='/login') {
            props.push(this.state.redirectTo)
        }
    }

    onFormChange = (value) => {
        this.setState({ formValues: value });
        if (value.iWantToRegister){
            this.props.logoutAndUnauth()
            if (value.password != value.repeatPassword) {
                this.setState({ error: 'passwords don\'t match' });
            } else {
                if (value.firstName === '') {
                    this.setState({ error: 'First name is required' });
                } else {
                    if (value.lastName === '') {
                        this.setState({ error: 'Last name is required' });
                    } else this.setState({ error: '' });
                }
            }
        }
    };

    extractRedirect = (string) => {
        const match = string.match(/next=(.*)/);
        return match ? match[1] : '/';
    };

    submit = () => {

        const values = this.state.formValues
        if (values) {
            console.log('register', values.iWantToRegister)
            
            if (values.iWantToRegister){
                if (this.state.error === '') {
                    // register
                    this.props.register(values.email, values.password, values.firstName, values.lastName)
                }               
            } else {
                // log in
                this.props.loginWithCredentials(values.email, values.password)  
            }    
        }
    };

    render() {
        return (
            <Login 
                statusText={this.props.statusText}
                submit={this.submit}
                formValues={this.state.formValues}
                loginForm={this.loginForm}
                onFormChange={this.onFormChange}
                isAuthenticating={this.props.isAuthenticating}
                error={this.state.error}/>
        );
    }
}

const mapStateToProps = ({users}) => {
    return {
        isAuthenticated: users.isAuthed,
        isAuthenticating: users.isFetchingActive,
        statusText: users.statusText
    };
};

export default connect(
    mapStateToProps, 
    (dispatch) => bindActionCreators({
        ...userActionCreators,
        push,
      }, dispatch)
)(LoginContainer);
export { LoginContainer as LoginContainerNotConnected };
