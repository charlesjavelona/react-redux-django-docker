export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(response.statusText);
    error.response = response;
    throw error;
}

export function parseJSON(response) {
    return response.json();
}

export function checkToken () {
    if (!getLocalToken()) return false
    else return true
  }
  
  export function getLocalToken () {
    return localStorage.getItem('token')
  }
  
  export function setLocalToken (token) {
    localStorage.setItem('token', token)
  }
  
  export function unsetLocalToken () {
    localStorage.removeItem('token')
  }