// users
export const UNAUTH_USER = 'UNAUTH_USER'
// const REMOVE_FETCHING_USER = 'REMOVE_FETCHING_USER'
export const REMOVE_FETCHING_ACTIVE_USER = 'REMOVE_FETCHING_ACTIVE_USER'
// const REQUEST_USER = '/user/REQUEST'
export const REQUEST_ACTIVE_USER = '/user/REQUEST_ACTIVE'
export const FAILURE_ACTIVE_USER = '/user/FAILURE_ACTIVE'
export const REQUEST_REGISTER = '/user/REQUEST_REGISTER'
export const RECEIVE_REGISTER = '/user/RECEIVE_REGISTER'
export const FAILURE_REGISTER = '/user/FAILURE_REGISTER'
export const FAILURE_ACTIVE_USER_TOKEN = '/user/FAILURE_ACTIVE_USER_TOKEN'

// const RECEIVE_USER = '/user/RECEIVE'
export const RECEIVE_ACTIVE_USER = '/user/RECEIVE_ACTIVE'
export const FAILURE_USER = '/user/FAILURE'

// posts
export const REQUEST_POSTS = '/posts/REQUEST'
export const RECEIVE_POSTS = '/posts/RECEIVE'
export const FAILURE_POSTS = '/posts/FAILURE'
export const RECEIVE_ALL_POSTS = '/posts/RECEIVE_ALL'

// user posts

export const REQUEST_USER_POSTS = '/userPosts/REQUEST'
export const RECEIVE_USER_POSTS = '/userPosts/RECEIVE'
export const FAILURE_USER_POSTS = '/userPosts/FAILURE'
export const RECEIVE_ONE_POST = '/userPosts/RECEIVE_ONE'
export const ADD_USER_POST = '/userPosts/ADD'