import { getLocalToken } from 'helpers/utils'
import { schema, normalize } from 'normalizr'
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'

// constants
import {REQUEST_USER_POSTS, RECEIVE_USER_POSTS, FAILURE_USER_POSTS, 
  RECEIVE_ONE_POST, ADD_USER_POST} from '../constants'


const nodeSchema = new schema.Entity('nodes')
const nodesSchema = new schema.Entity('userNodes',
  { nodes: [nodeSchema],
  }
  )

// Action creators

export function getUserPosts (id) {
  return {
    [CALL_API]: {
      endpoint: `userPosts/${id}`,
      method: 'GET',
      headers: { 'Authorization': `Bearer ${getLocalToken()}` },
      types: [
        REQUEST_USER_POSTS,
        {
          type: RECEIVE_USER_POSTS,
          payload: (action, state, res) => {
            return getJSON(res).then((json) => {
              // console.log(json)
              return normalize(json, nodesSchema)
            })
          },
        },
        FAILURE_USER_POSTS,
      ],
    },
  }
}

export function addUserPost (body) {
  return {
    [CALL_API]: {
      endpoint: '/db/answers/',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      types: [
        ADD_USER_POST,
        {
          type: RECEIVE_ONE_POST,
          payload: (action, state, res) => {
            return getJSON(res).then((json) => normalize(json, nodeSchema))
          },
        },
        FAILURE_USER_POSTS,
      ],
      body: JSON.stringify(body),
    },
  }
}

// initial state

const initialState = {
  isFetching: true,
  error: '',
  postIds: [],
}

// Reducers
export default function userPosts (state = initialState, action) {
  switch (action.type) {
    case REQUEST_USER_POSTS:
      return {
        ...state,
        isFetching: true,
      }
    case RECEIVE_USER_POSTS:
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          postIds: action.payload.entities.userNodes[action.payload.result].nodes,
        }
    case RECEIVE_ONE_POST:
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          postIds: _.union([action.payload.result], state.postIds),
        }
    case FAILURE_USER_POSTS:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
      }
    default :
      return state
  }
}
