import { getLocalToken } from 'helpers/utils'
import { schema, normalize } from 'normalizr'
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'

// constants
import { REQUEST_POSTS, RECEIVE_POSTS, FAILURE_POSTS, RECEIVE_USER_POSTS, 
  RECEIVE_ALL_POSTS, RECEIVE_ONE_POST  } from '../constants'


const userSchema = new schema.Entity('users')
const commentSchema = new schema.Entity('comments', {
  commenter: userSchema,
})
const postSchema = new schema.Entity('posts', {
  author: userSchema,
  comments: [commentSchema],
})

// Action creators

export function getPost (id) {
  return {
    [CALL_API]: {
      endpoint: `/db/posts/${id}`,
      method: 'GET',
      headers: { 'Authorization': `Bearer ${getLocalToken()}` },
      types: [
        REQUEST_POSTS,
        {
          type: RECEIVE_POSTS,
          payload: (action, state, res) => {
            return getJSON(res).then((json) => normalize(json, postSchema))
          },
        },
        FAILURE_POSTS,
      ],
    },
  }
}

export function getPosts () {
  return {
    [CALL_API]: {
      endpoint: `/db/posts`,
      method: 'GET',
      headers: { 'Authorization': `Bearer ${getLocalToken()}` },
      types: [
        REQUEST_POSTS,
        {
          type: RECEIVE_ALL_POSTS,
          payload: (action, state, res) => {            
            const result = getJSON(res).then((data) => {
              return data
            })
            console.log(result)
            return result
          },
        },
        FAILURE_POSTS,
      ],
    },
  }
}

// initial state

const initialState = {
  isFetching: true,
  error: '',
  postIds: [],
}

function post (nodes) {
  for (var node in nodes) {
    if (nodes.hasOwnProperty(node)) {
      if (nodes[node].nlu) {
        const nlu = JSON.parse(nodes[node].nlu)
        nodes[node].nlu = nlu
      }
    }
  }
  return nodes
}

// Reducers
export default function posts (state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTS:
      return {
        ...state,
        isFetching: true,
      }
    case RECEIVE_POSTS:
    case RECEIVE_ALL_POSTS:
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...post(action.payload.entities.nodes),
          postIds: _.union([action.payload.result], state.postIds),
        }
    case RECEIVE_USER_POSTS:
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...post(action.payload.entities.nodes),
          postIds: _.union(action.payload.entities.userNodes[action.payload.result].nodes, state.postIds),
        }
    case RECEIVE_ONE_POST:
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: '',
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...post(action.payload.entities.nodes),
          postIds: _.union([action.payload.result], state.postIds),
        }
    case FAILURE_POSTS:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
      }
    default :
      return state
  }
}
