import React from 'react'
import { PropTypes } from 'prop-types'

const Navbar = ({homeClass, protectedClass, loginClass, goToIndex, goToProtected, logout, goToLogin, isAuthed}) => (
    <nav className="navbar navbar-default">
        <div className="container-fluid">
            <div className="navbar-header">
                <button type="button"
                    className="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#top-navbar"
                    aria-expanded="false"
                >
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                </button>
                <a className="navbar-brand" onClick={goToIndex}>
                    Django React Redux Demo
                </a>
            </div>
            <div className="collapse navbar-collapse" id="top-navbar">
                {isAuthed ?
                    <ul className="nav navbar-nav navbar-right">
                        <li className={homeClass}>
                            <a className="js-go-to-index-button" onClick={goToIndex}>
                                <i className="fa fa-home" /> Home
                            </a>
                        </li>
                        <li className={protectedClass}>
                            <a className="js-go-to-protected-button" onClick={goToProtected}>
                                <i className="fa fa-lock" /> Protected
                            </a>
                        </li>
                        <li>
                            <a className="js-logout-button" onClick={logout}>
                                Logout
                            </a>
                        </li>
                    </ul>
                    :
                    <ul className="nav navbar-nav navbar-right">
                        <li className={homeClass}>
                            <a className="js-go-to-index-button" onClick={goToIndex}>
                                <i className="fa fa-home" /> Home
                            </a>
                        </li>
                        <li className={loginClass}>
                            <a className="js-login-button" onClick={goToLogin}>
                                <i className="fa fa-home" /> Login
                            </a>
                        </li>
                    </ul>
                }
            </div>
        </div>
    </nav>
)

Navbar.PropTypes = {
    homeClass: PropTypes.string.isRequired,
    protectedClass: PropTypes.string.isRequired, 
    loginClass: PropTypes.string.isRequired,
}

export default Navbar