import React from 'react'
import { PropTypes } from 'prop-types'
const Home = ({userName, goToProtected}) => (
    <div className="container">
        <div className="margin-top-medium text-center">
            
        </div>
        <div className="text-center">
            <h1>Django React Redux Demo</h1>
            <h4>Hello, {userName || 'guest'}.</h4>
        </div>
        <div className="margin-top-medium text-center">
            <p>Attempt to access some <a onClick={goToProtected}><b>protected content</b></a>.</p>
        </div>
    </div>
)
Home.PropTypes = {
    userName: PropTypes.string,
    goToProtected: PropTypes.func.isRequired,
}
export default Home