import React from 'react'
import { PropTypes } from 'prop-types'
import t from 'tcomb-form';
import classNames from 'classnames';

const Login = ({statusText, submit, 
    formValues, onFormChange, isAuthenticating, error}) => {
    
    const Form = t.form.Form;
    
    const LoginStruct = t.struct({
        email: t.String,
        password: t.String,
        repeatPassword: t.String,
        firstName: t.String,
        lastName: t.String,
        iWantToRegister: t.Boolean,
    });
    
    const LoginFormOptions = {
        auto: 'placeholders',
        help: <i>Hint: a@a.com / qw</i>,
        fields: {
            password: {
                type: 'password'
            },
            repeatPassword: {
                type: formValues.iWantToRegister ? 'password' : 'hidden',
                
            },
            firstName :{
                type: formValues.iWantToRegister ? 'string' : 'hidden',
            },
            lastName :{
                type: formValues.iWantToRegister ? 'string' : 'hidden',
            },
        },
        hasError: error !== '' && formValues.iWantToRegister,
        error: <i>{error}</i>
    };

    let status = null;
    if (statusText) {
        const statusTextClassNames = classNames({
            'alert': true,
            'alert-danger': statusText.indexOf('Error') != -1,
            'alert-success': statusText.indexOf('Error') === -1
        });

        status = (
            <div className="row">
                <div className="col-sm-12">
                    <div className={statusTextClassNames}>
                        {statusText}
                    </div>
                </div>
            </div>
        );
    }
    
    return <div className="container login">
        <h1 className="text-center">Login</h1>
        <div className="login-container margin-top-medium">
            {status}
            <form onSubmit={(e) => {
                    e.preventDefault();
                    submit()
                }}>
                <Form
                    type={LoginStruct}
                    options={LoginFormOptions}
                    value={formValues}
                    onChange={onFormChange}
                />
                <button disabled={isAuthenticating || (formValues.iWantToRegister && error !== '')}
                    type="submit"
                    className="btn btn-default btn-block"
                >
                    Submit
                </button>
            </form>
        </div>
    </div>
    }

Login.PropTypes = {
    statusText: PropTypes.string.isRequired,
    formValues: PropTypes.object.isRequired,
    LoginFormOptions: PropTypes.object.isRequired,
    submit: PropTypes.func.isRequired,
    onFormChange: PropTypes.func.isRequired,
    isAuthenticating: PropTypes.bool.isRequired,
    register: PropTypes.bool.isRequired,
}
export default Login