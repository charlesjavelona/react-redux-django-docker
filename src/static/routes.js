import React from 'react';
import { Route, Switch } from 'react-router';
import { HomeContainer, LoginContainer, ProtectedContainer, NotFoundContainer } from 'containers';
import requireAuthentication from 'helpers/requireAuthentication';

const getRoutes = () => ( 
    <Switch> 
        <Route exact path="/" component={HomeContainer} />
        <Route path="/login" component={LoginContainer} />
        <Route path="/protected" component={requireAuthentication(ProtectedContainer)} />
        <Route path="*" component={NotFoundContainer} /> 
    </Switch>)

export default getRoutes;
