import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import { RootContainer } from 'containers';
import * as reducers from 'reduxModules'
import { createStore, compose, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { routerReducer, syncHistoryWithStore, routerMiddleware } from 'react-router-redux'
import { apiMiddleware } from 'redux-api-middleware'
import injectTapEventPlugin from 'react-tap-event-plugin'
import { getLocalToken, setLocalToken, unsetLocalToken } from 'helpers/utils'

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

// this will be usefull if we make the app isomorphic
const initialState = window.__REDUX_STATE__

const target = document.getElementById('root');

const history = createHistory();

// this enables see routing events in redux logs and potentially do routing with redux actions (not doing it now)
const routingMiddleware = routerMiddleware(history)

// reducers define what happens to the redux store when each event is dispatched
const reducer = combineReducers({...reducers, routing: routerReducer})

// thunk allows to wrap reducers in dispatch function so in components we don't have to do dispatch(someFunc()) every time, we just do someFunc()
const enhancer = compose(
    // Middleware you want to use in development:
    applyMiddleware(apiMiddleware, thunk, routingMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f
  )

  // creating redux store
export const store = createStore(
    reducer,
    initialState,
    enhancer,
  )

const node = (
    <RootContainer store={store} history={history} />
);

// const node = <div> helo</div>

// const token = getLocalToken()

// if (token !== null) {
//    // store.dispatch(authLoginUserSuccess(token, user));

// }

ReactDOM.render(node, target);
